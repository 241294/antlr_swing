tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog      : (expr)*;

expr returns [Integer out]

	      : INT                      {$out = getInt($INT.text);}
	      //operations
        | ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        //additional operations
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out,$e2.out);}
        | ^(ROOT  e1=expr e2=expr) {$out = root($e1.out,$e2.out);}
        //print
        | ^(PRINT e1=expr)         {print($e1.out);}
        //variables
        | (i1=ID)                  {$out = getSymbol($i1.text);}
        | ^(PODST i1=ID e1=expr)   {setSymbol($i1.text, $e1.out);}
        | ^(VAR   i1=ID )          {newSymbol($i1.text);}
        
        ;
        
        