package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols.*;

public class MyTreeParser extends TreeParser {

	protected tb.antlr.symbolTable.GlobalSymbols globals = new tb.antlr.symbolTable.GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected void print(Object object){
    	if(object!=null)System.out.println(object.toString());
    }
    
    protected void newSymbol(String name) {
    	globals.newSymbol(name);
    }
    protected void setSymbol(String name, int value)
    {
    	globals.setSymbol(name, value);
    }
    protected Integer getSymbol(String name) {
    	return globals.getSymbol(name);
    }
    protected Integer divide(int a, int b) {
    	if(b!=0)return a/b;
    	else System.out.println("divide by 0 error");
    	return null;
    }
    protected Integer power(int a, int b) {
    	return (int)Math.pow(a, b);
    }
    protected Integer root(int a, int b) {
    	return (int)Math.pow(a, 1.0/b);
    }
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
